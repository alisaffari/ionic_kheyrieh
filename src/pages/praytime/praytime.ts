import { ENV } from './../../app/app.constant';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { HomePage } from '../home/home';


@IonicPage()
@Component({
  selector: 'page-praytime',
  templateUrl: 'praytime.html',
})
export class PraytimePage {

  Net;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams
              ) {
    if (ENV.Online == 1)
      this.Net = true;
    if (ENV.Online == 0)
      this.Net = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PraytimePage');
  }
  gotohome(){
    this.navCtrl.push(HomePage);
  }

}
