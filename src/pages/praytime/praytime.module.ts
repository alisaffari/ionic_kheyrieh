import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PraytimePage } from './praytime';

@NgModule({
  declarations: [
    PraytimePage,
  ],
  imports: [
    IonicPageModule.forChild(PraytimePage),
  ],
})
export class PraytimePageModule {}
