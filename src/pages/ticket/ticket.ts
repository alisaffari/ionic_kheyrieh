import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { BehaviorSubject } from 'rxjs';
import { TicketsPage } from '../tickets/tickets';

@IonicPage()
@Component({
  selector: 'page-ticket',
  templateUrl: 'ticket.html',
  providers: [ApiProvider]
})
export class TicketPage {

  @ViewChild(Content) content: Content;
  @ViewChild('chat_input') messageInput: ElementRef;
  msgList = [];
  msg_data: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  editorMsg = '';
  id;
  title;

  constructor(public navCtrl: NavController, public navParams: NavParams, private api:ApiProvider) {
    this.id = navParams.get('id');
    this.title = navParams.get('title');
    api.MyTickets.subscribe((val) => {
      this.msg_data.next(val);
    });
    api.getTicket(this.id);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TicketPage');
    this.msg_data.subscribe((val) => {
      this.msgList = val;
    });
  }

  onFocus() {
    this.content.resize();
    this.scrollToBottom();
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content.scrollToBottom) {
        this.content.scrollToBottom();
      }
    }, 400)
  }

  gototickets() {
    this.navCtrl.push(TicketsPage);
  }

  sendMsg() {
    this.api.newMsg(this.id,this.editorMsg);
    this.editorMsg = '';
  }

}
