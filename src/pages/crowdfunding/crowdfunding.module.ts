import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CrowdfundingPage } from './crowdfunding';

@NgModule({
  declarations: [
    CrowdfundingPage,
  ],
  imports: [
    IonicPageModule.forChild(CrowdfundingPage),
  ],
})
export class CrowdfundingPageModule {}
