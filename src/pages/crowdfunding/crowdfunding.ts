import { Storage } from '@ionic/storage';
import { ENV } from './../../app/app.constant';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { HomePage } from '../home/home';
import { CrowdfundingdetailPage } from '../crowdfundingdetail/crowdfundingdetail';
import { ApiProvider } from '../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-crowdfunding',
  templateUrl: 'crowdfunding.html',
  providers: [ApiProvider]
})
export class CrowdfundingPage {

  Campaigns;
  MyCampaigns;
  login;

  sec: string = "all";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private api: ApiProvider,
    private storage: Storage,
    public eventCtrl: Events
    ) {
      this.login = api.checkLogin();
      if (ENV.Online == 1){
        api.getCampaigns();
        if (this.login){
          api.getprofile();
          api.id.subscribe((id) => {
            api.getMyCampaigns(id);
          });
        };
      };
      storage.get('Campaigns').then((val) => {
        this.Campaigns = JSON.parse(val);
      });
      storage.get('MyCampaigns').then((val) => {
        this.MyCampaigns = JSON.parse(val);
        console.log(this.MyCampaigns);
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CrowdfundingPage');
  }

  gotohome(){
    this.navCtrl.push(HomePage);
  }

  crowdfundingdetail(num){
    if (ENV.Online == 1)
      this.navCtrl.push(CrowdfundingdetailPage, {id: num});
  }

}
