import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HomePage } from '../home/home';
import { BehaviorSubject } from 'rxjs';
import persianJs from 'persianjs';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  providers: [ApiProvider]
})
export class ProfilePage {

  // Object Variables
  profile;
  ShouldPay;
  SOM;
  SOD;
  Transactions_data: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  Transactions;

  // Text Variables
  ButText;
  ChText;
  EditText = "ویرایش اطلاعات";
  SaveText = "ذخیره اطلاعات";
  ChangeText = "تغییر مقدار حق عضویت ماهیانه";

  // Mode Variables
  EditMode:boolean=true;
  ChangeMode:boolean=true;
  sec: string = "profile";

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public api: ApiProvider,
              public alertCtrl: AlertController
              ) {
    this.ButText = this.EditText;
    this.ChText = this.ChangeText;

    api.getprofile();
    api.shpay();
    api.getTransactions();

    api.Profile.subscribe((val) => {
      this.profile = val;
    });
    api.should_pay.subscribe((val) => {
      this.ShouldPay = val;
    });
    api.Transactions.subscribe((val) => {
      this.Transactions_data.next(val);
    });

    api.getTotal();

    api.SOD.subscribe((val) => {
      this.SOD = val;
    });
    api.SOM.subscribe((val) => {
      this.SOM = val;
    });
  }

  ionViewDidLoad() {
    this.Transactions_data.subscribe((val) => {
      this.Transactions = val;
    });
  }

  gotohome(){
    this.navCtrl.push(HomePage);
  }

  SaveButton() {
    if (this.ButText == this.EditText){
      this.ButText = this.SaveText;
      this.EditMode = false;
    }
    else if(this.ButText == this.SaveText){
      this.ButText = this.EditText;
      this.EditMode = true;
      this.alertCtrl.create({
        title: 'آیا از ویرایش اطلاعات مطمئن هستید؟',
        buttons: 
        [
          {
            text: 'تایید',
            handler: data => {
              this.api.updateProfile(this.profile);
            }
          },
          {
            text: 'انصراف'
          }
        ]
      }).present();
    }   
  }

  Change() {
    if (this.ChText == this.ChangeText){
      var SH = persianJs(this.ShouldPay).toEnglishNumber().toString();
      if (SH == 0){
        this.ChText = this.SaveText;
        this.ChangeMode = false;
      }
      else {
        this.alertCtrl.create({
          title: 'خطا',
          subTitle: 'برای تغییر حق عضویت، ابتدا باید باقی مانده حساب را تسویه نمایید.',
          buttons: ['بستن']
        }).present();
      }
    }
    else if(this.ChText == this.SaveText){
      this.ChText = this.ChangeText;
      this.ChangeMode = true;
      this.alertCtrl.create({
        title: 'آیا از ویرایش اطلاعات مطمئن هستید؟',
        buttons: 
        [
          {
            text: 'تایید',
            handler: data => {
              this.api.SetMembership(this.profile.Membership_Fee);
            }
          },
          {
            text: 'انصراف'
          }
        ]
      }).present();
    }   
  }

  Pay() {

  }

}
