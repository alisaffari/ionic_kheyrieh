import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NewsPage } from '../news/news';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-newsdetail',
  templateUrl: 'newsdetail.html',
})
export class NewsdetailPage {

  id;
  News;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private storage: Storage
            ) {
    this.id = navParams.get('id');
    storage.get('News').then((val) => {
      this.News = JSON.parse(val);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewsdetailPage');
  }

  gotoNews() {
    this.navCtrl.popTo(NewsPage);
  }

}
