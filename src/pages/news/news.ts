import { BehaviorSubject } from 'rxjs';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { ENV } from './../../app/app.constant';
import { NewsdetailPage } from '../newsdetail/newsdetail';
import { HomePage } from '../home/home';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
  providers: [ApiProvider]
})
export class NewsPage {

  News;
  News_data = new BehaviorSubject<any[]>([]);

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private api:ApiProvider, 
              private storage: Storage,
              private event: Events
              ) {
    if (ENV.Online == 1)
      api.getNews();
    this.storage.get('News').then((val) => {
      this.News_data.next(JSON.parse(val));
    });
    this.News_data.subscribe((val) => {
      this.News = val;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewsPage');
  }

  news(Oid) {
    this.navCtrl.push(NewsdetailPage, { id: Oid});
  }

  gotohome(){
    this.navCtrl.push(HomePage);
  }

}
