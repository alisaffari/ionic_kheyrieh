import { Component } from '@angular/core';
import { AlertController, App, IonicPage, NavController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { SignupPage } from '../signup/signup';
import { ForgetpasswordPage } from '../forgetpassword/forgetpassword';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
  providers: [AuthProvider]
})
export class SigninPage {

  public username: string;
  public password: string;

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public app: App,
    public Auth: AuthProvider
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPage');
  }

  login() {
    this.Auth.signin(this.username,this.password);
  }

  goToSignup() {
    this.navCtrl.push(SignupPage);
  }

  goToResetPassword() {
    this.navCtrl.push(ForgetpasswordPage);
  }

  gotohome(){
    this.navCtrl.push(HomePage);
  }
}
