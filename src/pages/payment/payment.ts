import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  PaymentReason;
  ReasonText:string;
  price:string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.PaymentReason = navParams.get('pay_reason');
    switch (this.PaymentReason){
      case 0:
        this.ReasonText = "پرداخت صدقه";
        break;
      case 1:
        this.ReasonText = "پرداخت فطریه";
        break;
      case 2:
        this.ReasonText = "پرداخت زکات";
        break;
      case 3:
        this.ReasonText = "پرداخت خمس";
        break;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }

  pay(price) {
    console.log(price + " DONE!")
  }

  gotohome(){
    this.navCtrl.push(HomePage);
  }

}
