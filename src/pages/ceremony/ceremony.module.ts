import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CeremonyPage } from './ceremony';

@NgModule({
  declarations: [
    CeremonyPage,
  ],
  imports: [
    IonicPageModule.forChild(CeremonyPage),
  ],
})
export class CeremonyPageModule {}
