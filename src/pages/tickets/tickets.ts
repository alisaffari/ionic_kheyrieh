import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events } from 'ionic-angular';
import { HomePage } from '../home/home';
import { TicketPage } from '../ticket/ticket';
import { ApiProvider } from '../../providers/api/api';
import { BehaviorSubject } from 'rxjs';
import { ENV } from './../../app/app.constant';

@IonicPage()
@Component({
  selector: 'page-tickets',
  templateUrl: 'tickets.html',
  providers: [ApiProvider]
})
export class TicketsPage {

  Tickets_data = new BehaviorSubject<any[]>([]);
  Tickets;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private api: ApiProvider, 
              public alertCtrl: AlertController,
              public event: Events
              ) {
    if (ENV.Online == 1){
      api.Tickets.subscribe((val) => {
        this.Tickets_data.next(val);
      });
      api.getprofile();
      api.getTickets();
    };

    if (ENV.Online == 0) {
      this.alertCtrl.create({
        title:'خطا',
        subTitle: 'اینترنت متصل نیست',
        buttons: ['بستن']
      }).present();
      this.navCtrl.push(HomePage);
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TicketsPage');
    if (ENV.Online == 1) {
      this.Tickets_data.subscribe((val) => {
        this.Tickets = val;
      });
    };
  }

  gotohome() {
    this.navCtrl.push(HomePage);
  }

  Ticket(num, t) {
    this.navCtrl.push(TicketPage, {id : num, title: t});
  }

  closeTicket(id) {
    if (ENV.Online == 1)
      this.api.closeTicket(id);
    else {
      this.alertCtrl.create({
        title:'خطا',
        subTitle: 'اینترنت متصل نیست',
        buttons: ['بستن']
      }).present();
    }
  }

  openTicket(id) {
    if (ENV.Online == 1)
      this.api.openTicket(id);
    else {
      this.alertCtrl.create({
        title:'خطا',
        subTitle: 'اینترنت متصل نیست',
        buttons: ['بستن']
      }).present();
    }
  }

  newTicket() {
    if (ENV.Online == 1) {
      const prompt = this.alertCtrl.create({
        title: 'ایجاد تیکت جدید',
        message: "لطفا عنوان تیکت خود را وارد نمائید: ",
        inputs: [
          {
            name: 'val',
            placeholder: 'عنوان یا موضوع تیکت'
          },
        ],
        buttons: [
          {
            text: 'انصراف',
            handler: data => {
              console.log(data);
            }
          },
          {
            text: 'ایجاد تیکت',
            handler: data => {
              this.api.newTicket(data.val);
            }
          }
        ]
      });
      prompt.present();
    }
    else {
      this.alertCtrl.create({
        title:'خطا',
        subTitle: 'اینترنت متصل نیست',
        buttons: ['بستن']
      }).present();
    }
  }

}
