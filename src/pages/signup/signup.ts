import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { SigninPage } from '../signin/signin';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
  providers: [AuthProvider]
})
export class SignupPage {

  username:string;
  password:string;
  first_name:string;
  last_name:string;
  Membership_Fee:string;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public Auth: AuthProvider
            ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  signup() {
    this.Auth.signup(this.username,this.password,this.first_name,this.last_name,this.Membership_Fee);
    console.log('User Created');
  }

  gotoLogin() {
    this.navCtrl.push(SigninPage);
  }

  gotohome(){
    this.navCtrl.push(HomePage);
  }

}
