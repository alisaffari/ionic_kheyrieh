import { ENV } from './../../app/app.constant';
import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { PraytimePage } from '../praytime/praytime';
import { PaymentPage } from '../payment/payment';
import { NewsPage } from '../news/news';
import { ApiProvider } from '../../providers/api/api';
import { ProfilePage } from '../profile/profile';
import { CrowdfundingdetailPage } from '../crowdfundingdetail/crowdfundingdetail';
import { SigninPage } from '../signin/signin';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [ApiProvider]
})
export class HomePage {

  Should_Pay;
  Campaigns;
  login;
  
  constructor(public navCtrl: NavController, 
              public api:ApiProvider,
              public storage: Storage,
              public event: Events
              ) {
    console.log('Constructor');
    this.login = api.checkLogin();
    if (ENV.Online == 1) {
      if (this.login) {
        api.getprofile();
        api.shpay();
        api.getCampaigns();
        api.should_pay.subscribe((val) => {
          this.Should_Pay = val + " تومان";
          console.log(this.Should_Pay);
        });
      }
      else if (!this.login)
        this.Should_Pay = 'لطفا وارد شوید';
    };
    if (ENV.Online == 0)
      this.Should_Pay = 'اینترنت متصل نیست';
    storage.get('Campaigns').then((val) => {
      this.Campaigns = JSON.parse(val);
    });
  }

  ionViewDidLoad() {
    console.log('ViewLoad');
  }

  gotoPray() {
    this.navCtrl.push(PraytimePage);
  }

  gotopay(num) {
    this.navCtrl.push(PaymentPage, {pay_reason: num});
  }

  gotoNews() {
    this.navCtrl.push(NewsPage);
  }

  gotoprofile(){
    this.login = this.api.checkLogin();
    if (this.login)
      this.navCtrl.push(ProfilePage);
    if (!this.login)
      this.navCtrl.push(SigninPage);
  }

  gotoCampaign(num) {
    this.navCtrl.push(CrowdfundingdetailPage, {id: num});
  }

}
