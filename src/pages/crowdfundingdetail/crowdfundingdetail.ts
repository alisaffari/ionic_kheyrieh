import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events } from 'ionic-angular';
import { CrowdfundingPage } from '../crowdfunding/crowdfunding';
import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-crowdfundingdetail',
  templateUrl: 'crowdfundingdetail.html',
  providers: [ApiProvider]
})
export class CrowdfundingdetailPage {

  id;
  uid;
  amount;
  Campaigns;
  login;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private storage: Storage, 
              private api:ApiProvider, 
              public alertCtrl: AlertController,
              public event: Events
              ) {
    this.login = api.checkLogin();
    this.id = navParams.get('id');
    storage.get('Campaigns').then((val) => {
      this.Campaigns = JSON.parse(val);
    });
    api.getprofile();
    api.id.subscribe((val) => {
      this.uid = val;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CrowdfundingdetailPage');
  }

  gotocrowdfunding(){
    this.navCtrl.push(CrowdfundingPage);
  }

  Donate() {
    console.log(this.Campaigns);
    this.Campaigns.forEach((item) => {
      if(item.objectId == this.id){
        if(item.isMoney){
          const prompt = this.alertCtrl.create({
            title: 'مشارکت در کمپین',
            message: "لطفا مبلغ مورد نظر خود جهت مشارکت در کادر زیر وارد نمائید:",
            inputs: [
              {
                name: 'val',
                placeholder: 'مبلغ(ریال)'
              },
            ],
            buttons: [
              {
                text: 'انصراف',
                handler: data => {
                  console.log('Cancel clicked');
                }
              },
              {
                text: 'پرداخت',
                handler: data => {
                  console.log('Saved clicked');
                }
              }
            ]
          });
          prompt.present();
      }
        else if(!item.isMoney){
          if(this.login) {
          const prompt = this.alertCtrl.create({
            title: 'مشارکت در کمپین',
            message: "لطفا تعداد مورد نظر خود جهت مشارکت در کادر زیر وارد نمائید:",
            inputs: [
              {
                name: 'val',
                placeholder: 'تعداد'
              },
            ],
            buttons: [
              {
                text: 'انصراف',
                handler: data => {
                  console.log(data);
                }
              },
              {
                text: 'مشارکت در کمپین',
                handler: data => {
                  this.api.donate(this.id, data.val, this.uid);
                }
              }
            ]
          });
          prompt.present();
        }
      }
      else if (!this.login){
        this.alertCtrl.create({
          title: 'خطا',
          subTitle: 'برای شرکت در این کمپین ابتدا باید وارد حساب کاربری خود شوید.',
          buttons: ['بستن']
        }).present();
      }
      }
    });
  }

}
