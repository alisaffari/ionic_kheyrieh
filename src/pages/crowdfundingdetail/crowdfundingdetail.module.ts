import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CrowdfundingdetailPage } from './crowdfundingdetail';

@NgModule({
  declarations: [
    CrowdfundingdetailPage,
  ],
  imports: [
    IonicPageModule.forChild(CrowdfundingdetailPage),
  ],
})
export class CrowdfundingdetailPageModule {}
