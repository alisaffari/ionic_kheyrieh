import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';

import { ProgressBarModule } from 'angular-progress-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DonatePage } from '../pages/donate/donate';
import { CrowdfundingPage } from '../pages/crowdfunding/crowdfunding';
import { AuthProvider } from '../providers/auth/auth';
import { SigninPage } from '../pages/signin/signin';
import { SignupPage } from '../pages/signup/signup';
import { ForgetpasswordPage } from '../pages/forgetpassword/forgetpassword';
import { PraytimePage } from '../pages/praytime/praytime';
import { PaymentPage } from '../pages/payment/payment';
import { NewsPage } from '../pages/news/news';
import { NewsdetailPage } from '../pages/newsdetail/newsdetail';
import { ProfilePage } from '../pages/profile/profile';
import { ApiProvider } from '../providers/api/api';
import { CrowdfundingdetailPage } from '../pages/crowdfundingdetail/crowdfundingdetail';
import { TicketsPage } from '../pages/tickets/tickets';
import { TicketPage } from '../pages/ticket/ticket';
import { NetworkProvider } from '../providers/network/network';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DonatePage,
    CrowdfundingPage,
    SigninPage,
    SignupPage,
    ForgetpasswordPage,
    PraytimePage,
    PaymentPage,
    NewsPage,
    NewsdetailPage,
    ProfilePage,
    CrowdfundingdetailPage,
    TicketsPage,
    TicketPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    ProgressBarModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DonatePage,
    CrowdfundingPage,
    SigninPage,
    SignupPage,
    ForgetpasswordPage,
    PraytimePage,
    PaymentPage,
    NewsPage,
    NewsdetailPage,
    ProfilePage,
    CrowdfundingdetailPage,
    TicketsPage,
    TicketPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Storage,
    AuthProvider,
    ApiProvider,
    Network,
    NetworkProvider
  ]
})
export class AppModule {}
