import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { NetworkProvider } from '../providers/network/network';
import { ENV } from './app.constant';

import { HomePage } from '../pages/home/home';
import { DonatePage } from '../pages/donate/donate';
import { CrowdfundingPage } from '../pages/crowdfunding/crowdfunding';
import { SigninPage } from '../pages/signin/signin';
import { SignupPage } from '../pages/signup/signup';
import { PraytimePage } from '../pages/praytime/praytime';
import { NewsPage } from '../pages/news/news';
import { ProfilePage } from '../pages/profile/profile';
import { TicketsPage } from '../pages/tickets/tickets';
import { ApiProvider } from '../providers/api/api';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, 
              public statusBar: StatusBar, 
              public splashScreen: SplashScreen,
              public events: Events,
              public network: Network,
              public networkProvider: NetworkProvider,
              public alertCtrl: AlertController,
              public api: ApiProvider
              ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.networkProvider.initializeNetworkEvents();
      var log = this.api.checkLogin();

      // Login Event
      if (log) {
        console.log('LOGIN TRUE');
        this.pages = [
          { title: 'صفحه اصلی', component: HomePage },
          { title: 'پرداخت صدقه و خیرات', component: DonatePage},
          { title: 'فصل مهربانی', component: CrowdfundingPage},
          { title: 'اوقات شرعی', component: PraytimePage},
          { title: 'اخبار', component: NewsPage },
          { title: 'پروفایل', component: ProfilePage},
          { title: 'پشتیبانی', component: TicketsPage}
        ];
      };

      // Not Login Event
      if (!log){
        console.log('LOGIN FALSE');
        this.pages = [
          { title: 'صفحه اصلی', component: HomePage },
          { title: 'پرداخت صدقه و خیرات', component: DonatePage},
          { title: 'فصل مهربانی', component: CrowdfundingPage},
          { title: 'اوقات شرعی', component: PraytimePage},
          { title: 'اخبار', component: NewsPage },
          { title: 'ورود به حساب', component: SigninPage},
          { title: 'عضویت در انجمن', component: SignupPage}
        ];
      };


	    // Offline event
			this.events.subscribe('network:offline', () => {
        console.log('network:offline ==> '+this.network.type);
        ENV.Online = 0;
        let alert = this.alertCtrl.create({
          title: 'اینترنت متصل نیست',
          subTitle: this.network.type,
          buttons: ['بستن']
        });
        alert.present();    
		  });

			// Online event
			this.events.subscribe('network:online', () => {
        console.log('network:online ==> '+this.network.type);
        ENV.Online = 1;
        let alert = this.alertCtrl.create({
          title: 'اینترنت متصل است',
          subTitle: this.network.type,
          buttons: ['بستن']
        });
        alert.present();         
      });
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
