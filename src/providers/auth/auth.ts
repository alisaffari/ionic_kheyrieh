import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ENV } from '../../app/app.constant';
import Axios from 'axios';
import { AlertController, NavController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { SigninPage } from './../../pages/signin/signin';
import { Storage } from '@ionic/storage';
import { ApiProvider } from '../api/api';

@Injectable()
export class AuthProvider {

  private parseAppId: string = ENV.parseAppId;
  private parseServerUrl: string = ENV.parseServerUrl;
  public errortext: string;

  constructor(public http: HttpClient,
              public alertCtrl: AlertController,
              public navCtrl: NavController,
              private storage: Storage,
              public api: ApiProvider
  ) {
    /* Axios Default Values */
    Axios.defaults.baseURL = this.parseServerUrl;
    Axios.defaults.headers.common ['X-Parse-Application-Id'] = this.parseAppId;
    Axios.defaults.headers.post['Content-Type'] = 'application/json';
  }

  public signin(username: string, password: string) {
    Axios.post('/login', {
      username: username,
      password: password
    }, {
      headers: {
        "X-Parse-Revocable-Session": "1"
      }
    })
    .then(response => {
      if (response.status == 200) {

        var data = response.data;
        ENV.objectID = data.objectId;
        ENV.Session_Token = data.sessionToken;
        ENV.username = data.username;
        ENV.first_name = data.first_name;
        ENV.last_name = data.last_name;
        ENV.father_name = data.father_name;
        ENV.National_Code = data.National_Code;
        ENV.birthday = data.birthday;
        ENV.Membership_Fee = data.Membership_Fee;
        ENV.birth_place = data.birth_place;
        ENV.Address = data.Address;
        this.storage.set('profile', data);
        this.storage.set('Login', '1');
        this.storage.set('SToken', data.sessionToken);
        
        const alert = this.alertCtrl.create({
          title: 'خوش آمدید',
          subTitle: 'به صفحه اصلی اپلیکیشن هدایت خواهید شد',
          buttons: ['بستن']
        });
        alert.present();
        this.navCtrl.push(HomePage);
      }
    })
    .catch((err) => {
      
    });
  }

  public signup (
    username: string,
    password: string, 
    first_name:string,
    last_name: string,
    Membership_Fee: string
  ) {
    Axios.post('/users', {
      username: username,
      password: password,
      first_name: first_name,
      last_name: last_name,
      Membership_Fee: Membership_Fee,
      National_Code: password
    }, {
      headers: {
        "X-Parse-Revocable-Session": "1"
      }
    })
    .then(res => {
      if (res.status == 201) {

        var data = res.data;
        ENV.Session_Token = data.sessionToken;
        ENV.objectID = data.objectId;
        this.storage.set('Id',data.objectId);
        this.storage.set('SToken', data.sessionToken);
        this.storage.set('Login', '1');

        const alert = this.alertCtrl.create({
          title: 'انجام شد',
          subTitle: 'ثبت نام با موفقیت انجام شد',
          buttons: ['بستن']
        });
        alert.present();
        this.navCtrl.push(SigninPage);
      }
      else {
        this.ERRORHanlde(res.status);
      }
    })
  }

  public signout() {
    ENV.Session_Token = '';
    ENV.objectID = '';
    this.storage.set('Login', '0');
  }

  public ERRORHanlde(errornum: Number){
    switch (errornum) {
      case 200:
        this.errortext = "شماره همراه وارد نشده است";
        break;
      case 201:
        this.errortext = "کدملی وارد نشده است";
        break;
      case 202:
        this.errortext = "یک حساب کاربری با این شماره همراه ثبت شده است";
        break;
      case 209:
        this.errortext = "شما وارد نشده اید، لطفا ابتدا وارد شوید یا ثبت نام کنید";
        break;
    
      default:
        break;
    }

    const alert = this.alertCtrl.create({
      title: 'خطا',
      subTitle: this.errortext,
      buttons: ['بستن']
    });
    alert.present();

  }

}