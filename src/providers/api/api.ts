import { AlertController, Events } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { ENV } from '../../app/app.constant';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import Axios from 'axios';
import persianJs from 'persianjs';
import Jalali from 'jalaali-js';

@Injectable()
export class ApiProvider {

  private parseAppId: string = ENV.parseAppId;
  private parseServerUrl: string = ENV.parseServerUrl;
  public id = new BehaviorSubject<string[]>([]);
  public should_pay = new BehaviorSubject<string[]>([]);
  public SOM = new BehaviorSubject<string[]>([]);
  public SOD = new BehaviorSubject<string[]>([]);
  public isLogin = new BehaviorSubject<string[]>([]);
  public Profile = new BehaviorSubject<any[]>([]);
  public Transactions = new BehaviorSubject<any[]>([]);
  public Tickets = new BehaviorSubject<any[]>([]);
  public MyTickets = new BehaviorSubject<any[]>([]);
  public Online: boolean;
  public log;

  constructor(public alertCtrl: AlertController,
              private storage: Storage,
              public eventCtrl: Events
    ) {
      /* ------------------ Axios Default Values ------------------ */
      Axios.defaults.baseURL = this.parseServerUrl;
      Axios.defaults.headers.common ['X-Parse-Application-Id'] = this.parseAppId;
      Axios.defaults.headers.post['Content-Type'] = 'application/json';

      /* ------------------ Storage Initialize ------------------ */
      storage.ready().then(() => {});
    }

  /* ------------------ Get Profile Data (Local) ------------------ */
  public getprofile() {
    this.isLogin.subscribe((val) => {
      this.log = val;
      if (this.log == '1'){
        this.storage.get('profile').then((profile) => {
          profile.Membership_Fee = persianJs(profile.Membership_Fee.toString()).englishNumber().toString();
          this.Profile.next(profile);
          this.id.next(profile.objectId);
        });
      }
    });
  }
  /* ------------------ Set Profile Data (Server) ------------------ */
  public updateProfile(profile) {
    this.storage.get('SToken').then((val) => {
      Axios.put('/users/'+ profile.objectId, {
        username: profile.username,
        email: profile.email,
        birth_place: profile.birth_place,
        father_name: profile.father_name,
        last_name: profile.last_name,
        Address: profile.Address,
        National_Code: profile.National_Code,
        first_name: profile.first_name,
        birthday: profile.birthday
      }, {
        headers: {
          'X-Parse-Session-Token': val
        }
      })
      .then((res) => {
        console.log(res);
        Axios.get('/users/'+profile.objectId)
        .then((response) => {
          this.storage.set('profile', JSON.stringify(response.data))
          .then(() => {
            this.getprofile();
          });
        })
        .catch ((er) => {
          console.error(er);
        });
      })
      .catch((err) => {
        console.error(err);
        if (err.code == 209){
          this.alertCtrl.create({
            title: 'خطا',
            subTitle: 'از اپلیکیشن خارج شوید و مجددا وارد شوید. سپس دوباره امتحان کنید.',
            buttons: ['بستن']
          }).present();
        }
      });
    });
  }
  /* ------------------ Set Profile Data (Server) ------------------ */
  public SetMembership(amount) {
    this.storage.get('SToken').then((val) => {
      this.id.subscribe((id) => {
      Axios.put('/users/'+ id, {
        Membership_Fee: amount
      }, {
        headers: {
          'X-Parse-Session-Token': val
        }
      })
      .then((res) => {
        console.log(res);
        Axios.get('/users/'+id)
        .then((response) => {
          this.storage.set('profile', JSON.stringify(response.data))
          .then(() => {
            this.getprofile();
          });
        })
        .catch ((er) => {
          console.error(er);
        });
      })
      .catch((err) => {
        console.error(err);
        if (err.code == 209){
          this.alertCtrl.create({
            title: 'خطا',
            subTitle: 'از اپلیکیشن خارج شوید و مجددا وارد شوید. سپس دوباره امتحان کنید.',
            buttons: ['بستن']
          }).present();
        }
      });
      });
    });
  }
  /* ------------------ Get TotalPay Values (Server) ------------------ */
  public getTotal() {
    this.getprofile();
    this.id.subscribe((id) => {
      Axios.post('/functions/TotalPay', {
        id: id
      })
      .then((res) => {
        res.data.result.SOM = persianJs(res.data.result.SOM.toString()).englishNumber().toString() + ' تومان';
        res.data.result.SOD = persianJs(res.data.result.SOD.toString()).englishNumber().toString() + ' تومان';
        this.SOD.next(res.data.result.SOD);
        this.SOM.next(res.data.result.SOM);
      })
      .catch((err) => {
        console.error(err);
      });
    });
  }
  /* ------------------ Get Sh_Pay Value (Server) ------------------ */
  public shpay() {
    this.getprofile();
    this.id.subscribe((val) => {
      console.log('ID: ' + val);
      Axios.post('/functions/shpay', {
        id: val
        }, {
        headers: {
        "X-Parse-Revocable-Session": "1"
          }
        })
        .then(response => {
          response.data.result = persianJs(response.data.result.toString()).englishNumber().toString();
          this.should_pay.next(response.data.result);
          console.log('API SHPAY: ' + response.data.result);
        }).catch((err) => {
          console.log(err);
        });
    });
  }
  /* ------------------ Check Login (Local) ------------------ */
  public checkLogin(): boolean {
    var x: boolean;
    this.storage.get('Login').then((val)=>{
      if (val == '1'){
        x = true;
      }
      else
        x = false;
    });
    return x;
  }

  /* ------------------ Get News Data (Server) ------------------ */
  public getNews(){
    var nes = [];
      Axios.get('/classes/News?order=-createdAt')
      .then((res) => {
        nes = res.data.results;
        this.storage.set('News', JSON.stringify(nes));
      })
      .catch((err) => {
        console.log(err);
      });
  }

  // Transactions
  public getTransactions() {
    this.getprofile();
    this.id.subscribe((val) => {
      Axios.post('/functions/Transactions', {
        id: val
      }).then((response) => {
        response.data.result.forEach(item => {
          var SDate: Date;
          SDate = new Date(Date.parse(item.createdAt));
          var Jalal = Jalali.toJalaali(
            SDate.getFullYear(),
            SDate.getMonth() + 1,
            SDate.getDate()
          );
          var T = Jalal.jy.toString() + "/" + Jalal.jm.toString() + "/" + Jalal.jd;
          item.createdAt = persianJs(T).englishNumber().toString();
          switch(item.Reason){
            case 0:
              item.Reason = "پرداخت حق عضویت";
              break;
            case 1:
              item.Reason = "پرداخت فطریه";
              break;
            case 2:
              item.Reason =  "پرداخت زکات";
              break;
            case 3:
              item.Reason = "پرداخت خمس";
              break;
          }
          item.Value = persianJs(item.Value.toString()).englishNumber().toString() + " تومان";
        });
        this.storage.set('Transactions', JSON.stringify(response.data.result));
        this.Transactions.next(response.data.result);
      });
    });
    this.storage.get('Transactions').then((val) => {
      this.Transactions.next(JSON.parse(val));
    });
  }

  /* ------------------ Get Campaigns Data (Server) ------------------ */
  public getCampaigns() {
    Axios.post('/functions/Campaigns')
      .then((res) => {
        res.data.result.forEach(item => {
          item.updatedAt = (100.0 * parseFloat(item.Progress))/ parseFloat(item.Target_Unit);
          if (item.updatedAt > 100.0){
            item.updatedAt = 100.0;
          }
          item.Progress = persianJs(item.Progress).englishNumber().toString();
          item.Target_Unit = persianJs(item.Target_Unit).englishNumber().toString();
          var SDate: Date;
          var EDate: Date;
          SDate = new Date(Date.parse(item.Start_Date.iso));
          EDate = new Date(Date.parse(item.End_Date.iso));
          var Jalal = Jalali.toJalaali(
          SDate.getFullYear(),
          SDate.getMonth() + 1,
          SDate.getDate()
          );
          var Jalal1 = Jalali.toJalaali(
            EDate.getFullYear(),
            EDate.getMonth() + 1,
            EDate.getDate()
            );
          var T = Jalal.jy.toString() + "/" + Jalal.jm.toString() + "/" + Jalal.jd;
          var T1 = Jalal1.jy.toString() + "/" + Jalal1.jm.toString() + "/" + Jalal1.jd;
          item.Start_Date.iso = persianJs(T).englishNumber().toString();
          item.End_Date.iso = persianJs(T1).englishNumber().toString();
        });
        this.storage.set('Campaigns', JSON.stringify(res.data.result));
        console.log(res.data.result);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  /* ------------------ Get My Campaigns Data (Server) ------------------ */
  public getMyCampaigns(id) {
    Axios.post('functions/MyCampaigns', {
      id: id
    })
    .then((response) => {
      response.data.result.forEach(item => {
        item.Help_Number = persianJs(item.Help_Number.toString()).englishNumber().toString();
      });
      this.storage.set('MyCampaigns', JSON.stringify(response.data.result));
    })
    .catch((err) => {
      console.log(err);
    });
  }

  public donate(cid, amount, uid) {
    Axios.post('/functions/Donate',{
      uid: uid,
      cid: cid,
      amount: amount
    }).then((res) => {
      if (res.data.result.length == 0){
        Axios.post('/classes/User_Campaigns', {
          Help_Number: parseInt(amount),
          User: {
            "__type":"Pointer",
            "className":"_User",
            "objectId": uid
          },
          Campaign: {
            "__type":"Pointer",
            "className":"Campaigns",
            "objectId": cid
          }
        }).then((response) => {
          console.log(response.data);
        }).catch((error) => {
          console.log(error);
        });
      }
    else {
      Axios.put('/classes/User_Campaigns/' + res.data.result[0].objectId.toString() , {
        "Help_Number": parseInt(res.data.result[0].Help_Number) + parseInt(amount)
      }).then ((response) => {
        console.log(response.data);
      }).catch((errorr) => {
        console.log(errorr);
      });
    }
  }).catch((err) => {
    console.log(err);    
  });
  }

  /* ------------------ Get Tickets Data (Server) ------------------ */
  public getTickets() {
    this.id.subscribe((uid) => {
      Axios.post('/functions/getTickets', {
        id: uid
      })
      .then((res) => {
        res.data.result.forEach(item => {
          var SDate: Date;
          SDate = new Date(Date.parse(item.createdAt));
          var Jalal = Jalali.toJalaali(
            SDate.getFullYear(),
            SDate.getMonth() + 1,
            SDate.getDate()
          );
          var T = Jalal.jy.toString() + "/" + Jalal.jm.toString() + "/" + Jalal.jd;
          item.createdAt = persianJs(T).englishNumber().toString();
        });
      this.storage.set('Tickets', JSON.stringify(res.data.result));
      })
      .catch((err) => {
        console.log(err);
      });
    });
    this.storage.get('Tickets').then((val) => {
      this.Tickets.next(JSON.parse(val));
    });
  }
  /* ------------------ Get One Ticket Data (Server) ------------------ */
  public getTicket(id) {
    Axios.post('/functions/getTicket', {
      id: id
    })
    .then((res) => {
      res.data.result.forEach(item => {
        var SDate: Date;
        SDate = new Date(Date.parse(item.createdAt));
        var Jalal = Jalali.toJalaali(
          SDate.getFullYear(),
          SDate.getMonth() + 1,
          SDate.getDate()
        );
        var T = Jalal.jy.toString() + "/" + Jalal.jm.toString() + "/" + Jalal.jd;
        item.createdAt = persianJs(T).englishNumber().toString();
      });
      this.storage.set(id,JSON.stringify(res.data.result));
      this.MyTickets.next(res.data.result);
    })
    .catch((err) => {
      console.log(err);
    });
    this.storage.get(id).then((val) => {
      this.MyTickets.next(JSON.parse(val));
    });
  }
  /* ------------------ Create New Ticket (Server) ------------------ */
  public newTicket(title) {
    this.id.subscribe((uid) => {
      Axios.post('/classes/Ticket', {
        isOpen: true,
        Title: title,
        User: {
          "__type":"Pointer",
          "className":"_User",
          "objectId": uid
        }
      })
      .then((res) => {
        console.log(res);
        this.getTicket(uid);
      })
      .catch((err) => {
        console.log(err);
      });
    });
  }
  /* ------------------ Send New Messege Ticket (Server) ------------------ */
  public newMsg(tid,text) {
    Axios.post('/classes/User_Ticket', {
      Text: text,
      Ticket: {
        "__type":"Pointer",
        "className":"Ticket",
        "objectId":tid
      },
      isAdmin: false
    })
    .then((res) => {
      console.log(res.data);
      this.getTicket(tid);
    })
    .catch((err) => {
      console.error(err);
    });
  }
  /* ------------------ Close Ticket (Server) ------------------ */
  public closeTicket(tid) {
    Axios.put('/classes/Ticket/'+tid, {
      isOpen: false
    })
    .then((res) => {
      console.log(res);
      this.getTickets();
    })
    .catch((err) => {
      console.error(err);
    })
  }
  /* ------------------ ReOpen Ticket (Server) ------------------ */
  public openTicket(tid) {
    Axios.put('/classes/Ticket/'+tid, {
      isOpen: true
    })
    .then((res) => {
      console.log(res);
      this.getTickets();
    })
    .catch((err) => {
      console.error(err);
    })
  }
}
